/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cuckoohashingproject;

import java.awt.RenderingHints.Key;

/**
 *
 * @author ACER
 */
public class CuckooHashing {

    static int max;
    static int a;
    static int[][] hashtable = new int[a][max];
    static int[] pos = new int[a];

    static void initTable() {
        for (int j = 0; j < max; j++) {
            for (int i = 0; i < a; i++) {
                hashtable[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        switch (function) {
            case 1:
                return key % max;
            case 2:
                return (key / max) % max;
        }
        return Integer.MIN_VALUE;
    }

    static void place(int key, int tableID, int cnt, int n) {
        if (cnt == n) {
            System.out.printf("%d unpositioned\n", key);
            System.out.printf("Cycle present. REHASH.\n");
            return;
        }
        for (int i = 0; i < a; i++) {
            pos[i] = hash(i + 1, key);
            if (hashtable[i][pos[i]] == key) {
                return;
            }
        }
        if (hashtable[tableID][pos[tableID]] != Integer.MIN_VALUE) {
            int dis = hashtable[tableID][pos[tableID]];
            hashtable[tableID][pos[tableID]] = key;
            place(dis, (tableID + 1) % a, cnt + 1, n);
        } else {
            hashtable[tableID][pos[tableID]] = key;
        }
    }
    static void printTable() {
        System.out.printf("Final hash tables:\n");
        for (int i = 0; i < a; i++, System.out.printf("\n")) {
            for (int j = 0; j < max; j++) {
                if (hashtable[i][j] == Integer.MIN_VALUE) {
                    System.out.printf("- ");
                } else {
                    System.out.printf("%d ", hashtable[i][j]);
                }
            }
        }
        System.out.printf("\n");
    }
    static void cuckoo(int keys[], int n) {
        initTable();
        for (int i = 0, cnt = 0; i < n; i++, cnt = 0) {
            place(keys[i], 0, cnt, n);
        }
        printTable();
    }
    public static void main(String[] args) {
        int keys_1[] = {6, 10, 16, 19, 26, 32, 53, 58, 75, 91};
        int n = keys_1.length;
        cuckoo(keys_1, n);
    }

}
